//
//  AlertFactory.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 05/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

public class AlertFactory: NSObject {
    
    class func cantConnectToWS() -> UIAlertController {
        let alert = UIAlertController(title: "Imposible actualizar", message: "Puede que el servidor no esté disponible en estos momentos o que usted no disponga de conexión a la red", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "De acuerdo", style: UIAlertActionStyle.Default, handler: nil))
        return alert;
    }
    
    class func malformedUrl() -> UIAlertController {
        let alert = UIAlertController(title: "Error en la URL", message: "La dirección web a la que apunta este objeto no es válida", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "De acuerdo", style: UIAlertActionStyle.Default, handler: nil))
        return alert;
    }
}
