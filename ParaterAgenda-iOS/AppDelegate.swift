//
//  AppDelegate.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 20/07/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WSDelegate {

    var window: UIWindow?
    
    var dbFilePath: NSString = NSString()


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        if self.initializeDb() {
            //NSLog("Copy Succesful")
        }
        
        let ws = WS()
        ws.delegate = self;
        ws.getPeople()
        ws.getFavWebsites()
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - FMDB
    
    let DATABASE_RESOURCE_NAME = "DBLite"
    let DATABASE_RESOURCE_TYPE = "db"
    let DATABASE_FILE_NAME = "DBLite.db"
    
    func initializeDb() -> Bool {
        
        let documentFolderPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        
        let dbfile = "/" + DATABASE_FILE_NAME;
        
        self.dbFilePath = documentFolderPath.stringByAppendingString(dbfile)
        
        let filemanager = NSFileManager.defaultManager()
        
        let backupDbPath = NSBundle.mainBundle().pathForResource(DATABASE_RESOURCE_NAME, ofType: DATABASE_RESOURCE_TYPE)
        
        if (backupDbPath == nil) {
            return false
        }
        
        if (filemanager.fileExistsAtPath(dbFilePath as String) ) {
            
            var error: NSErrorPointer = NSErrorPointer()
            let removeSuccessful = filemanager.removeItemAtPath(dbFilePath as String, error: error)
            if !removeSuccessful {
                //NSLog("remove failed: \(error.debugDescription)")
                return false
            } else {
                //NSLog("remove successful at path: \(dbFilePath as String)")
            }
        }
        if (!filemanager.fileExistsAtPath(dbFilePath as String) ) {

            var error: NSError?
            let copySuccessful = filemanager.copyItemAtPath(backupDbPath!, toPath:dbFilePath as String, error: &error)
            if !copySuccessful {
                //NSLog("copy failed: \(error?.localizedDescription)")
                return false
            } else {
                //NSLog("copy successful at path: \(dbFilePath as String)")
            }
            
        }
        return true
        
    }
    
    func WebService(_ws: WS, didFinishPeopleRequestWithPeople _people: [PersonWithType], withError _error: Bool) {
        if _error == true {
            
        } else {
            let (people, types) = PersonWithType.split(personWithTypeList: _people)
            PersonWithTypeDAO.deleteAll()
            PersonWithTypeDAO.insert(_people)
            PersonTypeDAO.deleteAll()
            PersonTypeDAO.insert(types)
        }
    }
    
    func WebService(_ws: WS, didFinishFavWebsiteRequestWithFavWebsite _favWebsites: [FavWebsite], withError _error: Bool) {
        if _error == true {
            
        } else {
            FavWebsiteDAO.deleteAll()
            FavWebsiteDAO.insert(_favWebsites)
        }

    }

}

