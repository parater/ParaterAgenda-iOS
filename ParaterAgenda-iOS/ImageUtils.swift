//
//  ImageUtils.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 21/07/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils {
    private class func _resizeImage(originalImage: UIImage, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, // context size
            false,      // opaque?
            0);      // image scale. 0 means "device screen scale"
        let context = UIGraphicsGetCurrentContext();
        
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        originalImage.drawInRect(CGRectMake(0, 0, size.width, size.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage
    }
    
    class func resizeImage(originalImage: UIImage, size: CGSize, mantainAspectRatio: Bool = false) -> UIImage {
        
        if mantainAspectRatio == false {
            return _resizeImage(originalImage, size: size);
        } else {
            // Compute new size that mantains aspect ratio
            let origImgWidth = originalImage.size.width
            let origImgHeight = originalImage.size.height
            
            let imgAspectRatio = origImgWidth / origImgHeight
            
            let maxWidth = size.width
            let maxHeight = size.height
            
            var i : CGFloat = 1
            
            var computedWidth = origImgWidth
            var computedHeight = origImgHeight
            
            // Compute new size that mantains aspect ratio
            while (computedHeight > maxHeight || computedWidth > maxWidth) {
                let computedAspectRatioAcum = i * imgAspectRatio
                computedWidth = origImgWidth / computedAspectRatioAcum
                computedHeight = origImgHeight / computedAspectRatioAcum
            }
            
            let newSize = CGSize(width: computedWidth, height: computedHeight)
            
            return _resizeImage(originalImage, size: newSize);
            
        }

    }
    
    
    
}