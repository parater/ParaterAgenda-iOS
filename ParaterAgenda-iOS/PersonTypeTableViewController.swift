//
//  Person Type TableViewController.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 03/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

class PersonTypeTableViewController: UITableViewController, WSDelegate {
    
    var personTypes: [PersonType] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.personTypes = PersonTypeDAO.getAll()
        self.drawLogo()
        self.refreshControl!.addTarget(self, action: "updateTable", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.personTypes = PersonTypeDAO.getAll()
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.drawLogo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.personTypes.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PersonTypeCell", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...
        let nameLabel = tableView.viewWithTag(1) as! UILabel
        
        nameLabel.text = self.personTypes[indexPath.row].type

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ToPeople", sender: self.personTypes[indexPath.row].type)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "ToPeople") {
            let personTableViewController = segue.destinationViewController as! PersonTableViewController
            let personType = sender as! String
            personTableViewController.currentPersonType = personType
        }
    }
    
    func drawLogo() -> Void {
        
        let logoImg = UIImage(named:"F.Parater_logo_vector-sin_admon-android.png");
        self.navigationItem.titleView = UIImageView(image: ImageUtils.resizeImage(logoImg!, size: self.navigationController!.navigationBar.frame.size, mantainAspectRatio: true))
    }
    
    func updateTable() -> Void {
        
        let ws = WS()
        ws.delegate = self;
        ws.getPeople()
        
    }
    
    func reloadData() -> Void {
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
        self.refreshControl!.endRefreshing()

    }
    
    func WebService(_ws: WS, didFinishPeopleRequestWithPeople _people: [PersonWithType], withError _error: Bool) {
        if _error == true {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                var alert = AlertFactory.cantConnectToWS()
                self.presentViewController(alert, animated: true, completion: nil)
            })
            self.refreshControl!.endRefreshing()
        } else {
            let (people, types) = PersonWithType.split(personWithTypeList: _people)
            PersonWithTypeDAO.deleteAll()
            PersonWithTypeDAO.insert(_people)
            PersonTypeDAO.deleteAll()
            let _types = Array(Set(types))
            PersonTypeDAO.insert(_types)
            
            self.personTypes = _types
            
            self.reloadData()
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
