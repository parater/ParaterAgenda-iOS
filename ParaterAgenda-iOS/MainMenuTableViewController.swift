//
//  MainMenuTableViewController.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 03/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

class MainMenuTableViewController: UITableViewController {
    
    var menuItems: [MenuItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuItems += [
            MenuItem(name: "Directorio", image: UIImage(named: "Contacts")!, segueName: "ToPersonType"),
            MenuItem(name: "Webs de interés", image: UIImage(named: "Link")!, segueName: "ToFavWebsites")
        ]
        
        self.drawLogo()
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.drawLogo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.menuItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MainMenuCell", forIndexPath: indexPath) as! UITableViewCell

        
        
        // Configure the cell...
        let newSize = CGSizeMake(40, 40);

        let imageView = cell.viewWithTag(1) as! UIImageView
        
        let nameLabel = cell.viewWithTag(2) as! UILabel
        
        let contactsImg = UIImage(named: "Contacts")
        
        imageView.image = self.menuItems[indexPath.row].image
        
        nameLabel.text = self.menuItems[indexPath.row].name

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier(self.menuItems[indexPath.row].segueName, sender: nil)
    }
    
    func drawLogo() -> Void {
        
        let logoImg = UIImage(named:"F.Parater_logo_vector-sin_admon-android.png");
        self.navigationItem.titleView = UIImageView(image: ImageUtils.resizeImage(logoImg!, size: self.navigationController!.navigationBar.frame.size, mantainAspectRatio: true))
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
