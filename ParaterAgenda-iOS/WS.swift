//
//  WS.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 04/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

@objc public protocol WSDelegate {
    optional func WebService(_ws: WS, didFinishPeopleRequestWithPeople _people: [PersonWithType], withError _error: Bool)
    
    optional func WebService(_ws: WS, didFinishFavWebsiteRequestWithFavWebsite _favWebsites: [FavWebsite], withError _error: Bool)

}

public class WS: NSObject {
    
    public var delegate: WSDelegate?
    
    func getRequest(URL _url: String) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(URL: NSURL(string: _url)!)
        request.HTTPMethod = "GET"
        request.timeoutInterval = 240
        return request
    }
    
    func getPeople() -> Void {
        let request = self.getRequest(URL: "http://192.168.1.36:9000/person/")
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            let _error: NSErrorPointer = NSErrorPointer()
            
            if (error != nil) {
                self.delegate?.WebService!(self, didFinishPeopleRequestWithPeople: [PersonWithType](), withError: true)
                return
            }
            let responseObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: _error)
            if (responseObject != nil) {
                let responseArray = responseObject! as! NSMutableArray
                let people = self.JSONArrayToPersonWithTypeArray(JSONArray: responseArray)
                self.delegate?.WebService!(self, didFinishPeopleRequestWithPeople: people, withError: false)
                return
            }
        }
        
    }
    
    func getFavWebsites() -> Void {
        let request = self.getRequest(URL: "http://192.168.1.36:9000/favWebsite/")
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            let _error: NSErrorPointer = NSErrorPointer()
            
            if (error != nil) {
                self.delegate?.WebService!(self, didFinishFavWebsiteRequestWithFavWebsite: [FavWebsite](), withError: true)
                return
            }
            let responseObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: _error)
            if (responseObject != nil) {
                let responseArray = responseObject! as! NSMutableArray
                let favWebsites = self.JSONArrayToFavWebsiteArray(JSONArray: responseArray)
                self.delegate?.WebService!(self, didFinishFavWebsiteRequestWithFavWebsite: favWebsites, withError: false)
                return
            }
        }
        
    }
    
    func JSONArrayToPersonWithTypeArray(JSONArray _jsonArray: NSMutableArray) -> [PersonWithType] {
        
        var peopleWithType: [PersonWithType] = []
        
        for var i = 0; i < _jsonArray.count; i++ {
            var dictionary:  NSDictionary = _jsonArray.objectAtIndex(i) as! NSDictionary
            var name = dictionary.objectForKey("name") as! String
            var surname1 = dictionary.objectForKey("surname1") as! String
            var surname2 = dictionary.objectForKey("surname2") as! String
            var phone = dictionary.objectForKey("phone") as! String
            var email = dictionary.objectForKey("email") as! String
            var type = dictionary.objectForKey("type") as! String
            let personWithType = PersonWithType(name: name, surname1: surname1, surname2: surname2, phone: phone, email: email, type: PersonType(type: type))
            peopleWithType.append(personWithType)
        }
        return peopleWithType
    }
    
    func JSONArrayToFavWebsiteArray(JSONArray _jsonArray: NSMutableArray) -> [FavWebsite] {
        
        var favWebsites: [FavWebsite] = []
        
        for var i = 0; i < _jsonArray.count; i++ {
            var dictionary:  NSDictionary = _jsonArray.objectAtIndex(i) as! NSDictionary
            var label = dictionary.objectForKey("label") as! String
            var url = dictionary.objectForKey("url") as! String
            
            let favWebsite = FavWebsite(label: label, URL: url)
            
            favWebsites.append(favWebsite)
        }
        return favWebsites
    }
    
    
    
}
