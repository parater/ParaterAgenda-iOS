//
//  FavWebsiteRepository.swift
//  FincasParater
//
//  Created by Victor Caballero on 06/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//


class FavWebsiteRepository: NSObject {
    
    static var urls: [FavWebsite] = []
    
    private class func initList() -> [FavWebsite] {
        var urls: [FavWebsite] = []
        
        urls += [
            FavWebsite(label: "FincasParater", URL: "http://fincasparater.eu/"),
            FavWebsite(label: "Idealista", URL: "http://idealista.com/")
        ]
    
        return urls
        
    }
    
    class func getList() -> [FavWebsite] {
        
        if self.urls.isEmpty {
            self.urls = initList()
        }
        
        return self.urls
    }

}
