//
//  MenuItem.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 03/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

class MenuItem: NSObject {
    
    var name: String = ""
    var image: UIImage = UIImage()
    var segueName: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(name _name: String, image _image: UIImage, segueName _segueName: String) {
        self.init()
        self.name = _name
        self.image = _image
        self.segueName = _segueName
    }
   
}
