//
//  FavWebsite.swift
//  FincasParater
//
//  Created by Victor Caballero on 06/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//


public class FavWebsite: NSObject {
    
    public var url: String = ""
    
    public var label: String = ""
    
    convenience init(label _label: String, URL _url: String) {
        self.init()
        self.label = _label
        self.url = _url
    }

}
