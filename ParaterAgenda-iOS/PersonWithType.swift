//
//  PersonWithType.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 04/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

public class PersonWithType: Person {
    
    public var type: PersonType = PersonType(type: "no-clasificado")
    
    override init() {
        super.init()
    }
    
    convenience init(name _name: String, surname1  _surname1: String, surname2 _surname2: String, phone _phone: String, email _email: String,type _type: PersonType) {
        self.init()
        self.name = _name;
        self.surname1 = _surname1
        self.surname2 = _surname2
        self.phone = _phone
        self.email = _email
        self.type = _type
    }
    
    public class func split(personWithTypeList _people: [PersonWithType]) -> ([PersonWithType], [PersonType]) {
        
        var people: [PersonWithType] = []
        var types: [PersonType] = []
        
        for p in _people {
            people.append(PersonWithType(name: p.name, surname1: p.surname1, surname2: p.surname2, phone: p.phone, email: p.email, type: p.type))
            types.append(p.type)
            
        }
        return (people, types)
    }

}
