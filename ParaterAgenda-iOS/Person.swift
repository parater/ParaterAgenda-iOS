//
//  Person.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 20/07/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

public class Person: NSObject {
    
    public var name: String = ""
    public var surname1: String = ""
    public var surname2: String = ""
    public var phone: String = ""
    public var email: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(name _name: String, surname1  _surname1: String, surname2 _surname2: String, phone _phone: String, email _email: String) {
        self.init()
        self.name = _name;
        self.surname1 = _surname1
        self.surname2 = _surname2
        self.phone = _phone
        self.email = _email
    }
}
