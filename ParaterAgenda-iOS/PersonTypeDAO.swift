//
//  PersonTypeDAO.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 04/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

class PersonTypeDAO: NSObject {
    
    class func getAll() -> [PersonType] {
        
        var personTypes: [PersonType] = []
        
        let db = DBManager.getInstance().getDatabase()
        
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let mainQuery = "SELECT type_name FROM PersonType GROUP BY type_name"
        
        let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsInArray: [])
        
        while (rsMain!.next() == true) {
            let personType = rsMain?.stringForColumn("type_name")
            
            personTypes.append(PersonType(type: personType!))
            
        }
        
        db.close()
        return personTypes
    }
    
    class func insert(types: [PersonType]) -> [PersonType] {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let _types = Array(Set(types))
        
        for personType in _types {
            
            let insertQuery = "INSERT INTO PersonType (type_name) VALUES (\"\(personType.type)\")"
            let insertSuccessful = db.executeUpdate(insertQuery, withArgumentsInArray: nil)
            if !insertSuccessful {
                println("insert failed: \(db.lastErrorMessage())")
            }
        }
        db.close()
        return _types
    }
    
    class func deleteAll() -> Void {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let delQuery = "DELETE FROM PersonType"
        let deleteSuccessful = db.executeUpdate(delQuery, withArgumentsInArray: nil)
        if !deleteSuccessful {
            println("delete failed: \(db.lastErrorMessage())")
        }
        db.close()
    }

}
