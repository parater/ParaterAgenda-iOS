//
//  PersonRepository.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 21/07/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import Foundation

class PersonRepository: NSObject {
    
    static var people: [Person] = []
    
    private class func initList() -> [Person] {
        var people: [Person] = []
    
        people += [Person(name: "Albert", surname1: "Navarro", surname2: "Llamazares", phone: "666036688", email: "vicaba@me.com")]
            
        people += [Person(name: "Luciano", surname1: "Caballero", surname2: "Hierro", phone: "933390993", email: "vicaba@me.com")]
        
        return people
        
    }
    
    class func getList() -> [Person] {
        
        if self.people.isEmpty {
            self.people = initList()
        }
        
        return self.people
        
    }

}
