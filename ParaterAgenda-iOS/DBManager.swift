//
//  DBManager.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 04/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

class DBManager: NSObject {
    
    static var mInstance: DBManager? = nil
    
    var db: FMDatabase
    
    override init() {
        let mainDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        self.db = db
        super.init()
    }
    
    class func getInstance() -> DBManager {
        
        if (DBManager.mInstance == nil) {
            DBManager.mInstance = DBManager()
        }
        
        return DBManager.mInstance!
    }
    
    func getDatabase() -> FMDatabase {
        return self.db
    }

}
