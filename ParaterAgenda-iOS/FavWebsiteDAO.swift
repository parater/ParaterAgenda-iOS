//
//  FavWebsiteDAO.swift
//  FincasParater
//
//  Created by Victor Caballero on 06/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

class FavWebsiteDAO: NSObject {
    
    class func getAll() -> [FavWebsite] {
        
        var favWebsites: [FavWebsite] = []
        
        let db = DBManager.getInstance().getDatabase()
        
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let mainQuery = "SELECT label, url FROM FavWebsite"
        
        let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsInArray: [])
        
        while (rsMain!.next() == true) {
            let label = rsMain?.stringForColumn("label")
            let url = rsMain?.stringForColumn("url")
            
            favWebsites.append(FavWebsite(label: label!, URL: url!))
            
        }
        
        db.close()
        return favWebsites
    }
    
    class func insert(favWebsites: [FavWebsite]) -> [FavWebsite] {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        for favWebsite in favWebsites {
            
            let insertQuery = "INSERT INTO FavWebsite (label, url) VALUES (\"\(favWebsite.label)\", \"\(favWebsite.url)\")"
            let insertSuccessful = db.executeUpdate(insertQuery, withArgumentsInArray: nil)
            if !insertSuccessful {
                println("insert failed: \(db.lastErrorMessage())")
            }
        }
        db.close()
        return favWebsites
    }
    
    class func deleteAll() -> Void {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let delQuery = "DELETE FROM FavWebsite"
        let deleteSuccessful = db.executeUpdate(delQuery, withArgumentsInArray: nil)
        if !deleteSuccessful {
            println("delete failed: \(db.lastErrorMessage())")
        }
        db.close()
    }

}
