//
//  PersonTableViewController.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 20/07/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit
import MessageUI

class PersonTableViewController: UITableViewController, MFMailComposeViewControllerDelegate, WSDelegate {
    
    var currentPersonType: String?
    
    private var emailController: MFMailComposeViewController = MFMailComposeViewController()
    
    private var people: [PersonWithType] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.people = PersonWithTypeDAO.getAll(self.currentPersonType!)
        self.drawLogo()
        self.refreshControl!.addTarget(self, action: "updateTable", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.people = PersonWithTypeDAO.getAll(self.currentPersonType!)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.drawLogo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.people.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reusableCell", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...
        
        let nameLabel = cell.viewWithTag(1) as! UILabel
        let btnCall = cell.viewWithTag(2) as! UIButton
        let btnComposeMail = cell.viewWithTag(3) as! UIButton
        
        // Set images to buttons
        let callImg = UIImage(named: "Call")
        
        // New size for images
        let newSize = CGSizeMake(30, 30);
        
        // Resize and set the image for the call button
        btnCall.setImage(ImageUtils.resizeImage(callImg!, size: newSize), forState: UIControlState.Normal)
        
        // Resize and set the image for the compose mail button
        let composeImg = UIImage(named: "Message")
        
        btnComposeMail.setImage(ImageUtils.resizeImage(composeImg!, size: newSize), forState: UIControlState.Normal)
        
        // Set the name of te person to the label
        nameLabel.text = people[indexPath.row].name
        //nameLabel.text = currentPersonType!
        return cell
    }
    
    @IBAction func btnComposeMailAction(sender: AnyObject) {
        
        let clickedCell = sender.superview??.superview as! UITableViewCell
        let clickedButtonPath = self.tableView.indexPathForCell(clickedCell)
        
        let recipients = [people[clickedButtonPath!.row].email]
        
        if MFMailComposeViewController.canSendMail() {
            self.emailController = MFMailComposeViewController()
            self.emailController.mailComposeDelegate = self
            self.emailController.setToRecipients([people[clickedButtonPath!.row].email])
            self.presentViewController(emailController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnCallAction(sender: AnyObject) {
        let clickedCell = sender.superview??.superview as! UITableViewCell
        let clickedButtonPath = self.tableView.indexPathForCell(clickedCell)
        
        let phone = "telprompt://\(people[clickedButtonPath!.row].phone)";
        NSLog(phone)
        let url:NSURL = NSURL(string:phone)!;
        
        // Make the call
        UIApplication.sharedApplication().openURL(url);
    }
    
    // Delegate methos for the MailController
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        NSLog("Cancel button clicked")
        self.emailController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func drawLogo() -> Void {
        
        let logoImg = UIImage(named:"F.Parater_logo_vector-sin_admon-android.png");
        self.navigationItem.titleView = UIImageView(image: ImageUtils.resizeImage(logoImg!, size: self.navigationController!.navigationBar.frame.size, mantainAspectRatio: true))
    }
    
    func updateTable() -> Void {
        
        let ws = WS()
        ws.delegate = self;
        ws.getPeople()
        
    }
    
    func reloadData() -> Void {
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
        self.refreshControl!.endRefreshing()
        
    }
    
    func WebService(_ws: WS, didFinishPeopleRequestWithPeople _people: [PersonWithType], withError _error: Bool) {
        if _error == true {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                var alert = AlertFactory.cantConnectToWS()
                self.presentViewController(alert, animated: true, completion: nil)
            })
            self.refreshControl!.endRefreshing()
        } else {
            let (people, types) = PersonWithType.split(personWithTypeList: _people)
            PersonWithTypeDAO.deleteAll()
            PersonWithTypeDAO.insert(_people)
            PersonTypeDAO.deleteAll()
            PersonTypeDAO.insert(types)
            
            //NSLog("currentType = \(self.currentPersonType!)")
            
            self.people = PersonWithTypeDAO.getAll(self.currentPersonType!)
            
            self.reloadData()
            
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
