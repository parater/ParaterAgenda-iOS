//
//  PersonWithTypeDAO.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 04/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//


class PersonWithTypeDAO: NSObject {
    
    class func getAll() -> [PersonWithType] {
        
        var people: [PersonWithType] = []
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let mainQuery = "SELECT name, surname1, surname2, phone, email, type FROM Person"
        
        let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsInArray: [])
        
        while (rsMain!.next() == true) {
            let name = rsMain?.stringForColumn("name")
            let surname1 = rsMain?.stringForColumn("surname1")
            let surname2 = rsMain?.stringForColumn("surname2")
            let phone = rsMain?.stringForColumn("phone")
            let email = rsMain?.stringForColumn("email")
            let typeQ = rsMain?.stringForColumn("type")
            
            people.append(PersonWithType(name: name!, surname1: surname1!, surname2: surname2!, phone: phone!, email: email!, type: PersonType(type: typeQ!)))
            
        }
        return people
        
    }
    
    class func getAll(type: String) -> [PersonWithType] {
        
        var people: [PersonWithType] = []
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let mainQuery = "SELECT name, surname1, surname2, phone, email, type FROM Person JOIN PersonType WHERE Person.type == PersonType.type_name AND PersonType.type_name == \"\(type)\""
        
        //NSLog("mainQuery = \(mainQuery)");
        
        let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsInArray: [])
        
        while (rsMain!.next() == true) {
            let name = rsMain?.stringForColumn("name")
            let surname1 = rsMain?.stringForColumn("surname1")
            let surname2 = rsMain?.stringForColumn("surname2")
            let phone = rsMain?.stringForColumn("phone")
            let email = rsMain?.stringForColumn("email")
            let typeQ = rsMain?.stringForColumn("type")
            
            people.append(PersonWithType(name: name!, surname1: surname1!, surname2: surname2!, phone: phone!, email: email!, type: PersonType(type: typeQ!)))
            
        }
        db.close()
        return people
        
    }
    
    class func deleteAll() -> Void {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        let delQuery = "DELETE FROM Person"
        let deleteSuccessful = db.executeUpdate(delQuery, withArgumentsInArray: nil)
        if !deleteSuccessful {
            println("delete failed: \(db.lastErrorMessage())")
        }
        db.close()
    }
    
    
    class func insert(people: [PersonWithType]) -> Void {
        
        let db = DBManager.getInstance().getDatabase()
        if (!db.open()) {
            NSLog("error opening db")
        }
        
        for person in people {
            
            let insertQuery = "INSERT INTO Person (name, surname1, surname2, phone, email, type) VALUES (\"\(person.name)\", \"\(person.surname1)\", \"\(person.surname2)\", \"\(person.phone)\", \"\(person.email)\", \"\(person.type)\")"
            let insertSuccessful = db.executeUpdate(insertQuery, withArgumentsInArray: nil)
            if !insertSuccessful {
                println("insert failed: \(db.lastErrorMessage())")
            }
        }
        db.close()
    }

}
