//
//  PersonType.swift
//  ParaterAgenda-iOS
//
//  Created by Victor Caballero on 03/08/15.
//  Copyright (c) 2015 Cobuc. All rights reserved.
//

import UIKit

public class PersonType: NSObject {
    
    public var type: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(type _type: String) {
        self.init()
        self.type = _type
    }
    
    override public func isEqual(object: AnyObject?) -> Bool {
        if let object = object as? PersonType {
            return type == object.type
        } else {
            return false
        }
    }
    
    override public var description: String {
        return type
    }
    
    override public var hash: Int {
        return type.hashValue
    }
   
}